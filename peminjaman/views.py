from django.shortcuts import render
from django.db import connection
import datetime
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

def peminjaman(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SET search_path to bike_sharing;")
    cursor.execute("SELECT * FROM sepeda WHERE status ='f'")

    sepeda_stasiun = cursor.fetchall()
    response['sepeda_stasiun'] = sepeda_stasiun

    return render(request, 'peminjaman.html', response)

def tambah_peminjaman(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path to bike_sharing;")
    if (request.method == "POST"):
        sepedaStasiun = request.POST['sepeda_stasiun']
        ktp = request.session['ktp']
        cursor.execute("SELECT no_kartu FROM anggota WHERE ktp='"+ktp+"';")
        no_kartu = cursor.fetchone()[0]
        cursor.execute("SELECT id_stasiun FROM sepeda WHERE nomor='"+sepedaStasiun+"';")
        id_stasiun = cursor.fetchone()[0]
        datetime_pinjam = str(datetime.datetime.strptime(str(datetime.datetime.now())[:18], '%Y-%m-%d %H:%M:%S'))
        cursor.execute("INSERT INTO peminjaman(no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun) VALUES("+"'"+no_kartu+"','"+datetime_pinjam+"','"+str(sepedaStasiun)+"','"+id_stasiun+"'"+");")
        cursor.execute("UPDATE sepeda SET status='F' WHERE nomor='"+sepedaStasiun+"';")
        messages.error(request, "Peminjaman berhasil dilakukan!")
        return HttpResponseRedirect('/peminjaman/daftar-peminjaman')

    else:
        messages.error(request, "Peminjaman gagal dilakukan.")
        return HttpResponseRedirect('/peminjaman/daftar-peminjaman')

def daftar_peminjaman(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path to bike_sharing;")
    response = {}
    if (request.session['role'] == 'petugas'):
        cursor.execute("SELECT * FROM peminjaman")
        peminjaman = cursor.fetchall()
        response['peminjaman'] = peminjaman
        print(response)
        return render(request, 'daftar_peminjaman.html', response)
    else:
        ktp = request.session['ktp']
        cursor.execute("SELECT * FROM peminjaman p, anggota a WHERE p.no_kartu_anggota = a.no_kartu AND a.ktp='"+ktp+"';")
        peminjaman = cursor.fetchall()
        response['peminjaman'] = peminjaman
        print(response)
        return render(request, 'daftar_peminjaman.html', response)