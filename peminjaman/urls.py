from django.urls import path
from peminjaman import views

urlpatterns = [
	path('', views.peminjaman, name='peminjaman'),
	path('/tambah-peminjaman', views.tambah_peminjaman, name='tambah_peminjaman'),
	path('/daftar-peminjaman', views.daftar_peminjaman, name='daftar_peminjaman')
]
