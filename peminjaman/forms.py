from django import forms
from .models import modelsPeminjaman

SEPEDA_STASIUN=[
    ('sepeda gunung', 'Sepeda Gunung'),
    ('sepeda fixie', 'Sepeda Fixie')
]
class formPeminjaman(forms.Form):
    sepeda_stasiun = forms.CharField(label='Sepeda stasiun',
                           widget=forms.Select(choices=SEPEDA_STASIUN))