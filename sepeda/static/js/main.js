/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

$(document).ready( function () {
    $('#mytable').DataTable();
} );

function pinjam(x){
  no_sepeda = x
  window.location.replace("/peminjaman")
}

function deleteList(x){
    $(".cd-popup").addClass("is-visible")
    $(".cd-popup").attr('id',x)
}


function updateSepeda(){
  nomor= $(".modal-body").attr('id')
  merk = $('#merkSepeda').val()
  if(merk==''){
      merk = $('#merkSepeda').attr('placeholder')
  }
  jenis = $('#jenisSepeda').val()
  if(jenis==''){
      jenis=$('#jenisSepeda').attr('placeholder')
  }
  status = $('#statusSepeda').val()
  stasiun = $('#inputStasiun').val()
  penyumbang = $('#penyumbangSepeda').val()


  $.ajax({
        url: "/sepeda/updatesepeda",
          datatype: 'json',
          data: {
              'nomor':nomor,
              'merk':merk,
              'jenis':jenis,
              'stasiun':stasiun,
              'status':status,
              'penyumbang':penyumbang,
          },
          method:'POST',
          success: function(result){
            location.reload()
            } ,
            error:function(a){
                alert(a)
                location.reload();
            }
      }
  );

}

function cancelFunction(){
    $('.cd-popup').removeClass('is-visible')
}

function yesFunction(){
    id = $(".cd-popup").attr('id')
    alert(id)
    $.ajax(
        {
          url: "/sepeda/deletesepeda",
            datatype: 'json',
            data: {'id':id},
            method:'POST',
            success: function(result){
              location.reload()
              }
        }
        );
}
function updateModal(x){
  id= $(".modal-body").attr('id',x)
  $(".spinner-border").addClass("true")
    $(".spinner-border").removeClass("false")
    $("#load").addClass("false")
    $("#load").removeClass("true")
    $.ajax(
        {
          url: "/sepeda/detailupdate",
            datatype: 'json',
            data: {'id':x},
            method:'POST',
            success: function(result){
            console.log(result)
            $(".spinner-border").addClass("false")
            $(".spinner-border").removeClass("true")
            $("#load").addClass("true")
            $("#load").removeClass("false")
            status = 'Tersedia'
            status_opt='<option>Tidak tersedia</option>'
            if(result.status==false){
                status ='Tidak tersedia'
                status_opt='<option>Tersedia</option>'
            }
            stasiun=''
            for(i=0; i<result.stasiun.length;i++){
                stasiun+='<option>'+result.stasiun[i]+'</option>'
            }
            penyumbang=''
            for(i=0; i<result.penyumbang.length;i++){
                penyumbang+='<option>'+result.penyumbang[i]+'</option>'

            }
            console.log(result.nama_stasiun)
            console.log(result.merk)
            $('#merkSepeda').attr('placeholder',result.merk)
            $('#jenisSepeda').attr('placeholder',result.jenis)
            $('#inputStasiun').html(
              `<option selected>`+result.nama_stasiun+`</option>`+
              stasiun
              )
            $('#statusSepeda').html(
                `<option selected>`+status+`</option>`+
                status_opt
                )
            $('#penyumbangSepeda').html(
                `<option selected>`+result.nama_penyumbang+`</option>`+
                penyumbang
            )
            }
        }
        );
}
