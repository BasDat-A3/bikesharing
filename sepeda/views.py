from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
# from .forms import FilterChoice, SortingChoice

# Create your views here.
def sepeda(request):
    response={}
    c = connection.cursor()
    c.execute('set search_path to bike_sharing,public')
    c.execute("SELECT nama FROM stasiun")

    result1 = c.fetchall()
    response['stasiun'] = result1
    print(response)
    return render(request, 'createsepeda.html',response)

def postsepeda(request):
    if(request.method=="POST"):
        merk = request.POST['merk']
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']

        print(status)
        statusNow = False
        if(str(status)=='Tersedia'):
            statusNow = True
        else:
            statusNow = False

        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')
        c.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp;")
        result = c.fetchone()
        no_kartu_penyumbang = result[0]

        c.execute("select id_stasiun from stasiun where nama='"+stasiun+"';")
        result1 = c.fetchone()
        id_stasiun = result1[0]

        c.execute("select nomor from sepeda order by cast (nomor as int) desc;")
        result2 = c.fetchone()
        nomor = int(result2[0]) +1

        c.execute("INSERT INTO sepeda(nomor,merk,jenis,status,id_stasiun,no_kartu_penyumbang) values("+"'"+str(nomor)+"','" +str(merk)+"','" + str(jenis) +"','" + str(statusNow) +"', '" + str(id_stasiun) +"', '" + str(no_kartu_penyumbang) +"' );")

        return HttpResponseRedirect('/sepeda')

    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/sepeda/createsepeda')


def listsepeda(request):
    response={}
    c = connection.cursor()
    c.execute('set search_path to bike_sharing,public')
    c.execute("select s.nama, p.nama, n.nomor, n.merk, n.jenis, n.status from sepeda as n, stasiun as s, person as p, anggota as a where n.no_kartu_penyumbang = a.no_kartu and a.ktp = p.ktp and n.id_stasiun = s.id_stasiun; ")
    result1 = c.fetchall()
    response['sepeda'] = result1
    print(response)
    return render(request, 'daftar_sepeda.html',response)


def updatesepeda(request):
    if(request.method=="POST"):
        merk = request.POST['merk']
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']
        nomor = request.POST['nomor']

        statusNow = False
        if(str(status)=='Tersedia'):
          statusNow = True
        else:
          statusNow = False

        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')

        c.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp")
        result = c.fetchone()
        no_kartu = result[0]

        c.execute("update sepeda set merk='"+merk+"', jenis='"+jenis+"', status='"+str(statusNow)+"', no_kartu_penyumbang='"+no_kartu+"' where nomor = '"+str(nomor)+"';")


        return HttpResponseRedirect('/sepeda')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/sepeda/updatesepeda')

@csrf_exempt
def detailupdate(request):
    if(request.method=="POST"):
        response={}
        sepeda= request.POST['id']
        c = connection.cursor()
        c.execute('set search_path to bike_station,public')
        c.execute("select * from sepeda where nomor='"+sepeda+"';")
        stasiun = c.fetchone()

        c.execute("select s.nama from stasiun as s, sepeda as a where a.nomor ='"+sepeda+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = c.fetchone()

        c.execute("select p.nama from person as p, anggota as a, sepeda as n where n.nomor='"+sepeda+"' and a.no_kartu=n.no_kartu_penyumbang and p.ktp=a.ktp;")
        result = c.fetchone()
        nama_penyumbang = result[0]

        c.execute("select jenis from sepeda where nomor='"+sepeda+"';")
        jenis = c.fetchone()

        c.execute("select status from sepeda where nomor='"+sepeda+"';")
        status = c.fetchone()

        c.execute("select merk from sepeda where nomor='"+sepeda+"';")
        merk = c.fetchone()

        c.execute("select p.nama from person as p, anggota as a where a.ktp=p.ktp")
        penyumbang = c.fetchall()

        c.execute("select nama from stasiun")
        stasiun = c.fetchall()
        response['stasiun'] = stasiun
        response['penyumbang'] = penyumbang
        response['sepeda'] = sepeda
        response['nama_stasiun'] = nama_stasiun
        response['nama_penyumbang'] = nama_penyumbang
        response['jenis'] = jenis[0]
        response['merk'] = merk[0]
        response['status'] = status[0]

        return JsonResponse(response)

    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/sepeda/updatesepeda')

@csrf_exempt
def deletesepeda(request):
    if(request.method=="POST"):
        nomor= request.POST['id']
        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')
        c.execute("delete from sepeda where nomor='"+nomor+"';")

        return HttpResponseRedirect('/sepeda')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/sepeda')
