from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
# from .forms import FilterChoice, SortingChoice

response = {}

def liststasiun(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select * from stasiun")
	result1 = c.fetchall()
	response['stasiun'] = result1
	#print(response)
	return render(request, 'daftar_stasiun.html',response)

def stasiun(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select nama from stasiun")

	result1 = c.fetchall()
	response['stasiun'] = result1
	print(response)
	return render(request, 'createStasiun.html',response)

def poststasiun(request):
	if(request.method=="POST"):
		nama = request.POST['nama']
		alamat = request.POST['alamat']
		longitude = request.POST['longitude']
		latitude = request.POST['latitude']

		c = connection.cursor()
		c.execute('set search_path to bike_sharing,public')
		c.execute("SELECT id_stasiun FROM stasiun")
		id_stasiun = c.fetchall()
		for row in id_stasiun:
			id_stasiun = row[0]

		c.execute("SELECT id_stasiun FROM stasiun ORDER BY cast (id_stasiun as int) DESC;")
		result = c.fetchone()
		id_stasiun = int(result[0]) +1

		print(id_stasiun)
		c.execute("INSERT INTO stasiun(id_stasiun,alamat,lati,longi,nama) VALUES ("+"'"+str(id_stasiun)+"','" +str(alamat)+"'," + str(latitude) +",'" + str(longitude) +"','" + str(nama)+"');")
		return HttpResponseRedirect('/stasiun')

	else:
		messages.error(request, "failed")
		return HttpResponseRedirect('/stasiun/createstasiun')

def updatestasiun(request):
	if(request.method=="POST"):
		print("hehe")
		nama = request.POST['nama']
		alamat = request.POST['alamat']
		longitude = request.POST['longitude']
		latitude = request.POST['latitude']
		id_stasiun= request.POST['id_stasiun']

		c = connection.cursor()
		c.execute('set search_path to bike_sharing,public')
		c.execute("update stasiun set nama='"+nama+"', alamat='"+alamat+"', lat='"+latitude+"', long='"+longitude+"' where id_stasiun = '"+id_stasiun+"';")

		messages.error(request, "success")
		return HttpResponseRedirect('/stasiun')

	else:
		messages.error(request, "failed")
		return HttpResponseRedirect('/stasiun')


@csrf_exempt
def deletestasiun(request):
	if(request.method=="POST"):
		id_stasiun= request.POST['id']
		c = connection.cursor()
		c.execute('set search_path to bike_sharing,public')
		c.execute("DELETE FROM stasiun WHERE id_stasiun='"+id_stasiun+"';")

		return HttpResponseRedirect('/stasiun')

	else:
		messages.error(request, "failed")
		return HttpResponseRedirect('/stasiun')

@csrf_exempt
def detailupdate(request):
    if(request.method=="POST"):
        response={}
        stasiun= request.POST['id']
        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')
        c.execute("SELECT * FROM stasiun WHERE id_stasiun='"+stasiun+"';")
        stasiun = c.fetchone()
        response['stasiun'] = stasiun

        return JsonResponse(response)

    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/stasiun')
