from django.urls import path
from stasiun import views
app_name = 'stasiun'

urlpatterns = [
	path('', views.liststasiun, name = 'stasiun'),
	path('/createstasiun',views.stasiun,name='createstasiun'),
	path('/updatestasiun/',views.updatestasiun,name='updatestasiun'),
	path('/poststasiun',views.poststasiun, name = 'poststasiun'),
    path('/deletestasiun',views.deletestasiun, name = 'deletestasiun'),
    path('/detailupdate',views.detailupdate, name = 'detailupdate'),
]
