from django.urls import path
from acara import views
app_name = 'acara'

urlpatterns = [
	path('', views.acara, name = 'acara'),
	path('/add_acara', views.add_acara, name = 'add_acara'),
	path('/post_acara', views.post_acara, name = 'post_acara'),
	path('delete_acara/', views.delete_acara, name='delete_acara'),
    path('update_acara/', views.update_acara, name='update_acara'),
    path('detail_update_acara/', views.detail_update_acara, name='detail_update_acara'),
	path('/sortAcaraDESC/',views.sortAcaraDESC, name = 'sortAcaraDESC'),
	path('/sortAcaraASC/', views.sortAcaraASC, name = 'sortAcaraASC'),
]
