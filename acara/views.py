from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

def acara(request):
    response = {}
    c = connection.cursor()
    c.execute('set search_path to bike_sharing,public')
    c.execute("select * from acara")
    
    result = c.fetchall()
    response['acara'] = result
   
    return render(request, 'daftar_acara.html',response)

def add_acara(request):
    if(request.session['role']=='petugas'):
        response={}
        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')
        c.execute("select nama from stasiun")
    
        result = c.fetchall()
        response['stasiun'] = result
    
        return render(request, 'add_acara.html', response)
    
    else:
        return HttpResponseRedirect('/')


def post_acara(request):
    if(request.method=="POST"):
        judul = request.POST['judul']    
        deskripsi = request.POST['deskripsi']
        gratis = request.POST['gratis']
        mulai =request.POST['mulai']
        selesai = request.POST['selesai']
        list_of_stasiun = request.POST.getlist('stasiun')
        fee = False
        if(gratis == 'gratis'):
            fee = True
        else:
            fee = False
        
        c = connection.cursor()
        c.execute('set search_path to bike_sharing,public')
        c.execute("select id_acara from acara order by cast (id_acara as int) desc;")
        res = c.fetchone()
        id_acara = int(res[0]) +1
        c.execute("INSERT INTO acara(id_acara,judul,tgl_mulai,is_free,tgl_akhir,deskripsi) values("+"'"+str(id_acara)+"','" 
            +str(judul)+"','"  + mulai +"'," + str(fee) +",'" + selesai + "','" + str(deskripsi) +"');")
        
        for i in list_of_stasiun:
            c.execute("select id_stasiun from stasiun  where nama ='"+i+"' ")
            id_stasiun = c.fetchone()
            c.execute("INSERT INTO acara_stasiun(id_stasiun,id_acara) values("+"'"+str(id_stasiun[0])+"','"  + str(id_acara) +"');")

        messages.error(request, "success")
        return HttpResponseRedirect('/acara')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/acara/add_acara')

@csrf_exempt
def update_acara(request):
    if(request.method == "POST"):
        id_acara = request.POST['id_acara']
        judul = request.POST['judul']
        deskripsi = request.POST['deskripsi']
        gratis = request.POST['gratis']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        list_of_stasiun = request.POST.getlist('stasiun[]')
        fee = False
        if(gratis == 'gratis'):
            fee = True
        else:
            fee = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("update acara set judul='"+judul+"', is_free='"+str(fee)+"', tgl_mulai='" +
                       mulai+"', tgl_akhir='"+selesai+"', deskripsi='"+deskripsi+"' where id_acara = '"+id_acara+"';")
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" +
                       id_acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        for i in range(0, len(list_stasiun)):
            id_stasiun_lama = ''
            id_stasiun = ''
            for i in nama_stasiun:
                print(i[0])
                cursor.execute(
                    "select id_stasiun from stasiun  where nama ='"+i[0]+"' ")
                id_stasiun_lama = cursor.fetchone()
                id_stasiun_lama = id_stasiun_lama[0]

            for j in list_stasiun:
                cursor.execute(
                    "select id_stasiun from stasiun  where nama ='"+j+"' ")
                id_stasiun = cursor.fetchone()
                id_stasiun = id_stasiun[0]

            cursor.execute("update acara_stasiun set id_stasiun='"+id_stasiun +
                           "' where id_acara='"+id_acara+"' and id_stasiun='"+id_stasiun_lama+"';")

        messages.error(request, "success")
        return HttpResponseRedirect('/acara')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara')


@csrf_exempt
def detail_update_acara(request):
    if(request.method == "POST"):
        response = {}

        acara = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("select * from acara where id_acara='"+acara+"';")
        id_acara = cursor.fetchone()
        cursor.execute("select nama from stasiun")
        stasiun = cursor.fetchall()
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" +
                       acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        response['stasiun'] = stasiun
        response['acara'] = id_acara
        response['nama_stasiun'] = nama_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara')

@csrf_exempt
def delete_acara(request):
    if(request.method == "POST"):

        id_acara = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("delete from acara where id_acara='"+id_acara+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/acara')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/acara')

def sortAcaraASC(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select * from acara order by judul ASC")
	result = c.fetchall()
	response['acara'] = result
	print(response)
	return render(request, 'daftar_acara.html',response)

def sortAcaraDESC(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select * from acara order by judul DESC")
	result = c.fetchall()
	response['acara'] = result
	print(response)
	return render(request, 'daftar_acara.html',response)


