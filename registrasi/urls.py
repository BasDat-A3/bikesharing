from django.urls import path
from registrasi import views

urlpatterns = [
	path('', views.registrasi, name = 'registrasi'),
	path('pickarole/', views.pickarole, name = 'pickarole'),
	#path('admin/', views.admin, name = 'admin'),
	path('signUpAnggota/', views.signUpAnggota, name = 'signUpAnggota'),
	path('signUpPetugas/', views.signUpPetugas, name = 'signUpPetugas'),
	path('afterSignup/petugas/', views.afterPetugasSignUp, name = 'petugasSignUp'),
	path('afterSignup/anggota/', views.afterAnggotaSignUp, name = 'anggotaSignUp'),
	path('afterLogin/', views.afterLogin, name = 'afterLogin'),
	path('logout/', views.logout, name = 'logout'),
]
