from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
from random import randint

# Create your views here.
def registrasi(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')

    return render(request,'registrasi.html')

def pickarole(request):
    return render(request, 'pickarole.html')

def signUpAnggota(request):
    return render(request, 'signUpAnggota.html')

#def admin(request):
#    return render(request, 'signup.html')    

def signUpPetugas(request):
    return render(request, 'signUpPetugas.html')

def login(request):
    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def afterLogin(request):
    cek_ktp_valid = str(request.POST.get("ktp"))
    #print(request.POST)
    #print("cek_ktp_valid : " + cek_ktp_valid)
    #print(cek_ktp_valid, "is of type", type(cek_ktp_valid))
    
    if( len(str(request.POST.get('ktp'))) != 9):
        print("masuk cek ktp panjang 9")
        messages.error(request, "KTP harus 9 digit bro")
        return HttpResponseRedirect('/registrasi/')
    
    email = request.POST['email']
    ktp=''

    ktpFormattingIndex = 0
    for i in range(9):
        if (i == 3 or i == 5):
            ktp = ktp + '-'
            ktpFormattingIndex += 1
            ktp = ktp + cek_ktp_valid[i]
        else:
            ktp = ktp + cek_ktp_valid[i]
            ktpFormattingIndex += 1

    print(ktp)

    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing, public')
    cursor.execute("select * from person where ktp='" + ktp + "' and email='" + email + "'")
    person = cursor.fetchone()

    if(person):
        #print("masuk person")
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        petugas = cursor.fetchone()
        if(petugas):
            cursor.execute("select * from person where ktp='"+ktp+"'")
            nama = cursor.fetchall()
            for row in nama:
                nama = row[0]
            request.session['nama'] = nama
            request.session['ktp'] = ktp
            request.session['email'] = email
            request.session['role'] = 'petugas'
            return HttpResponseRedirect('/penugasan')
        else:
            cursor.execute("select * from anggota where ktp='"+ktp+"'")
            anggota = cursor.fetchone()
            if(anggota):
                #print("masuk anggota")
                cursor.execute("select nama from person where ktp='"+ktp+"'")
                nama = cursor.fetchall()
                for row in nama:
                    nama = row[0]
                request.session['nama'] = nama
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['role'] = 'anggota'
                return HttpResponseRedirect('/sepeda')
            else:
                messages.error(request, "email atau ktp salah")
                return HttpResponseRedirect('/registrasi/')
    else:
        messages.error(request, "email atau ktp salah, belum terdaftar")
        return HttpResponseRedirect('/registrasi/')

def afterPetugasSignUp(request):
    cek_ktp_valid = str(request.POST.get("ktp"))
    #print(request.POST)
    #print("cek_ktp_valid : " + cek_ktp_valid)
    #print(cek_ktp_valid, "is of type", type(cek_ktp_valid))
    
    if( len(str(request.POST.get('ktp'))) != 9):
        print("masuk cek ktp panjang 9")
        messages.error(request, "KTP harus 9 digit bro")
        return HttpResponseRedirect('/registrasi/petugas/')
    
    namaLengkap = request.POST['namaLengkap']
    email = request.POST['email']
    nomorHP = request.POST['nomorHP']
    tanggalLahir = request.POST['tanggalLahir']
    alamat = request.POST['alamat']
    gaji='30000'
    ktp=''

    ktpFormattingIndex = 0
    for i in range(9):
        if (i == 3 or i == 5):
            ktp = ktp + '-'
            ktpFormattingIndex += 1
            ktp = ktp + cek_ktp_valid[i]
        else:
            ktp = ktp + cek_ktp_valid[i]
            ktpFormattingIndex += 1

    print(ktp)

    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select * from petugas where ktp = '" + ktp + "'")
    ktpSearch = cursor.fetchone()
    print(ktpSearch, "is of type", type(ktpSearch))
    
    if(ktpSearch):
        if (len(str(ktpSearch))>0):
            messages.error(request, "nomor KTP nya udah terregistrasi")
            return HttpResponseRedirect('/registrasi/petugas/')
    
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select * from person where email = '" + email + "'")
    emailSearch = cursor.fetchone()
    print(emailSearch, "is of type", type(ktpSearch))
    
    if(emailSearch):
        if (len(str(emailSearch))>0):
            messages.error(request, "alamat email nya udah terregistrasi")
            return HttpResponseRedirect('/registrasi/petugas/')

    inputValues = "'"+ ktp + "','" + email + "','" + namaLengkap + "','" + alamat + "','" + tanggalLahir + "','" + nomorHP + "'"
    inputQuery = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + inputValues + ")"
    cursor.execute(inputQuery)

    inputValues = "'" + ktp + "'," + gaji
    inputQuery = "INSERT INTO petugas (ktp,gaji) values (" + inputValues + ")"
    cursor.execute(inputQuery)

    messages.success(request, "" + namaLengkap + " berhasil di registrasi sebagai petugas")
    return HttpResponseRedirect('/registrasi/')

def afterAnggotaSignUp(request):
    cek_ktp_valid = str(request.POST.get("ktp"))
    print(request.POST)
    #print("cek_ktp_valid : " + cek_ktp_valid)
    print(cek_ktp_valid, "is of type", type(cek_ktp_valid))
    
    if( len(str(request.POST.get('ktp'))) != 9):
        print("masuk cek ktp panjang 9")
        messages.error(request, "KTP harus 9 digit bro")
        return HttpResponseRedirect('/registrasi/signUpAnggota/')
    
    namaLengkap = request.POST['namaLengkap']
    email = request.POST['email']
    nomorHP = request.POST['nomorHP']
    tanggalLahir = request.POST['tanggalLahir']
    alamat = request.POST['alamat']
    no_kartu = getNomorKartu()
    saldo = '0'
    points = '0'
    ktp=''

    ktpFormattingIndex = 0
    for i in range(9):
        if (i == 3 or i == 5):
            ktp = ktp + '-'
            ktpFormattingIndex += 1
            ktp = ktp + cek_ktp_valid[i]
        else:
            ktp = ktp + cek_ktp_valid[i]
            ktpFormattingIndex += 1

    print(ktp)

    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select * from anggota where ktp = '" + ktp + "'")
    ktpSearch = cursor.fetchone()
    print(ktpSearch, "is of type", type(ktpSearch))
    
    if(ktpSearch):
        if (len(str(ktpSearch))>0):
            messages.error(request, "nomor KTP nya udah terregistrasi")
            return HttpResponseRedirect('/registrasi/signUpAnggota/')
    
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select * from person where email = '" + email + "'")
    emailSearch = cursor.fetchone()
    print(emailSearch, "is of type", type(ktpSearch))
    
    if(emailSearch):
        if (len(str(emailSearch))>0):
            messages.error(request, "alamat email nya udah terregistrasi")
            return HttpResponseRedirect('/registrasi/signUpAnggota/')

    inputValues = "'"+ ktp + "','" + email + "','" + namaLengkap + "','" + alamat + "','" + tanggalLahir + "','" + nomorHP + "'"
    inputQuery = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + inputValues + ")"
    cursor.execute(inputQuery)

    inputValues = "'" + no_kartu + "'," + saldo + "," + points + ",'" + ktp + "'"
    inputQuery = "INSERT INTO anggota (no_kartu,saldo, points, ktp) values (" + inputValues + ")"
    cursor.execute(inputQuery)

    messages.success(request, "" + namaLengkap + " berhasil di registrasi sebagai anggota")
    return HttpResponseRedirect('/registrasi/')

def getNomorKartu():
    nomorKartuRandom = randint(10000000, 99999999)
    nomorKartuRandom = str(nomorKartuRandom)
    nomorKartuResult = ''
    nomorkartuFormattingIndex = 0

    #for i in range(8):
    #    if (nomorkartuFormattingIndex == 3 or nomorkartuFormattingIndex == 5):
    #        nomorKartuResult = nomorKartuResult + '-'
    #        nomorKartuResult = nomorKartuResult + nomorKartuRandom[i]
    #        nomorkartuFormattingIndex += 1
    #    else:
    #        nomorKartuResult = nomorKartuResult + nomorKartuRandom[i]
    #        nomorkartuFormattingIndex += 1
    

    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("SELECT * from anggota where no_kartu='" + nomorKartuResult + "'")
    nomorKartuSearch = cursor.fetchone()
    
    if (nomorKartuSearch):
        if len(nomorKartuSearch)>0:
            getNomorKartu()
    else:
        return nomorKartuResult
