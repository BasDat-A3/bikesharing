from django.urls import path
from laporan import views

urlpatterns = [
	path('daftarLaporan/', views.daftarLaporan, name = 'daftarLaporan'),
	path('sortLaporanASC/', views.sortLaporanASC, name = 'sortLaporanASC'),
	path('sortLaporanDESC/', views.sortLaporanDESC, name = 'sortLaporanDESC'),
	path('filterByDenda/', views.filterByDenda, name = 'filterByDenda'),
	#path('daftarLaporan/<int:page>', views.daftarLaporanPagination, name = 'daftarLaporanPagination'),
]