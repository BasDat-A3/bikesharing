from django.shortcuts import render
from django.shortcuts import render_to_response
from django.db import connection
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
# def daftarLaporanPagination(request, page):
#     ktp = request.session['ktp']
#     cursor = connection.cursor()
#     offset = (page * 5) - 5
#     offset = str(offset)
#     limit = page * 5
#     limit = str(limit)
#     cursor.execute('set search_path to bike_sharing')
#     cursor.execute("select l.id_laporan, l.no_kartu_anggota, l.datetime_pinjam, SUM(p.denda), l.status from peminjaman p ,anggota a, laporan l where a.no_kartu = l.no_kartu_anggota and a.no_kartu = p.no_kartu_anggota group by l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, l.status LIMIT "+ limit +"OFFSET "+ offset +";")
#     allLaporan = cursor.fetchall()

#     laporanDict = {'all_posts': allLaporan}
#     print(allLaporan)
#     return render(request, 'daftarLaporan.html', laporanDict)

def daftarLaporan(request):
    ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select l.id_laporan, l.no_kartu_anggota, l.datetime_pinjam, SUM(p.denda), l.status from peminjaman p ,anggota a, laporan l where a.no_kartu = l.no_kartu_anggota and a.no_kartu = p.no_kartu_anggota group by l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, l.status;")
    allLaporan = cursor.fetchall()

    paginator = Paginator(allLaporan, 5) # Show 5 contacts per page

    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request, 'daftarLaporan.html', {'all_posts': contacts})

    # page = request.GET.get('page', 1)
    # paginator = Paginator(allLaporan, 10)
    # try:
    #     users = paginator.page(page)
    # except PageNotAnInteger:
    #     users = paginator.page(1)
    # except EmptyPage:
    #     users = paginator.page(paginator.num_pages)

    # return render(request, 'daftarLaporan.html', { 'all_posts': users })

    # laporanDict = {'all_posts': allLaporan}
    # print(allLaporan)
    # return render(request, 'daftarLaporan.html', laporanDict)

def sortLaporanASC(request):
    ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, SUM(p.denda), l.status from peminjaman p ,anggota a, laporan l where a.no_kartu = l.no_kartu_anggota and a.no_kartu = p.no_kartu_anggota group by l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, l.status order by l.datetime_pinjam ASC;")
    allLaporan = cursor.fetchall()

    laporanDict = {'all_posts' : allLaporan}
    return render(request, 'daftarLaporan.html', laporanDict)

def sortLaporanDESC(request):
    ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, SUM(p.denda), l.status from peminjaman p ,anggota a, laporan l where a.no_kartu = l.no_kartu_anggota and a.no_kartu = p.no_kartu_anggota group by l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, l.status order by l.datetime_pinjam DESC;")
    allLaporan = cursor.fetchall()
    
    laporanDict = {'all_posts' : allLaporan}
    return render(request, 'daftarLaporan.html', laporanDict)

def filterByDenda(request):
    denda = request.POST.get("denda")
    print("denda ========== ")
    print(denda)
    #denda = int(stringDenda)
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, SUM(p.denda), l.status from peminjaman p ,anggota a, laporan l where a.no_kartu = l.no_kartu_anggota and a.no_kartu = p.no_kartu_anggota group by l.id_laporan, l.datetime_pinjam, l.no_kartu_anggota, l.status HAVING SUM(p.denda) > " + denda + " order by l.datetime_pinjam DESC;")
    allLaporan = cursor.fetchall()
    
    laporanDict = {'all_posts' : allLaporan}
    return render(request, 'daftarLaporan.html', laporanDict)