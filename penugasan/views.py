from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

def penugasan(request):
    if(request.session['role']=='petugas'):
        response = {}
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')

        cursor.execute("select pg.ktp, p.nama, pg.start_datetime, pg.end_datetime, s.nama from stasiun s, penugasan pg, person as p where s.id_stasiun = pg.id_stasiun and pg.ktp = p.ktp;")
        result = cursor.fetchall()
        response['penugasan'] = result

   
        return render(request, 'daftar_penugasan.html',response)
    
    else:
        return HttpResponseRedirect('/')

def add_penugasan(request):
    if(request.session['role']=='petugas'):
        response={}
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        
        cursor.execute("select nama from stasiun")
        result = cursor.fetchall()
        response['stasiun'] = result

        cursor.execute("select pg.ktp, p.nama from penugasan pg, person p where p.ktp = pg.ktp;")
        petugas = cursor.fetchall()
        response['petugas'] = petugas

    
        return render(request, 'add_penugasan.html', response)
    
    else:
        return HttpResponseRedirect('/')

def post_penugasan(request):
    if(request.method == 'POST'):
        petugas = request.POST['petugas']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        stasiun = request.POST['stasiun']
        i = petugas.split('-')
        ktp = "-".join(i[:3])
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("select id_stasiun from stasiun where nama ='"+stasiun+"';")
        id_stasiun = cursor.fetchone()
        cursor.execute("INSERT INTO penugasan(ktp,start_datetime,id_stasiun,end_datetime) values("+"'"+str(ktp)+"','" +mulai+"','" +str(id_stasiun[0]) +"','" + selesai + "');")
        
        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/penugasan/add_penugasan')

def sortPetugasASC(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select pg.ktp, p.nama, pg.start_datetime, pg.end_datetime, s.nama from stasiun s, penugasan pg, person as p where s.id_stasiun = pg.id_stasiun and pg.ktp = p.ktp order by ktp ASC")
	result = c.fetchall()
	response['penugasan'] = result
	print(response)
	return render(request, 'daftar_penugasan.html',response)

def sortPetugasDESC(request):
	response={}
	c = connection.cursor()
	c.execute('set search_path to bike_sharing,public')
	c.execute("select pg.ktp, p.nama, pg.start_datetime, pg.end_datetime, s.nama from stasiun s, penugasan pg, person as p where s.id_stasiun = pg.id_stasiun and pg.ktp = p.ktp order by ktp DESC")
	result = c.fetchall()
	response['penugasan'] = result
	print(response)
	return render(request, 'daftar_penugasan.html',response)