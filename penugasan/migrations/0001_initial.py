# Generated by Django 2.1.1 on 2019-05-06 12:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Penugasan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('petugas', models.CharField(max_length=100)),
                ('mulai', models.DateField()),
                ('selesai', models.DateField()),
                ('stasiun', models.CharField(max_length=100)),
            ],
        ),
    ]
