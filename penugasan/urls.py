from django.urls import path
from penugasan import views
app_name = 'penugasan'


urlpatterns = [
	path('', views.penugasan, name = 'penugasan'),
	path('/add_penugasan', views.add_penugasan, name = 'add_penugasan'),
	path('/post_penugasan', views.post_penugasan, name = 'post_penugasan'),

	path('/sortPetugasDESC/',views.sortPetugasDESC, name = 'sortPetugasDESC'),
	path('/sortPetugasASC/', views.sortPetugasASC, name = 'sortPetugasASC'),
]
