from django import forms
from .models import modelsVoucher

class formVoucher(forms.Form):
    nama = forms.CharField(label='Nama',
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukkan nama voucher'}))
    kategori = forms.CharField(label="Kategori",
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukkan kategori voucher'}))
    nilai_poin = forms.IntegerField(label='Nilai Poin',
                             widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Masukkan nilai poin'}))
    deskripsi = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukkan deskripsi voucher'}))
    jumlah = forms.IntegerField(label='Jumlah',
                                widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Masukkan jumlah voucher'}))