from django.db import models
from django.contrib.auth.models import User

class modelsVoucher(models.Model):
    nama = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
    nilai_poin = models.IntegerField()
    deskripsi = models.TextField()
    jumlah = models.IntegerField()