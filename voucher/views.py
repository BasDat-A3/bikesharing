from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator

def voucher(request):
    if (request.session['role'] == 'petugas'):
        response = {}
        cursor = connection.cursor()
        cursor.execute("SET search_path to bike_sharing;")
        cursor.execute("SELECT * FROM voucher")
        vouchers = cursor.fetchall()
        response['voucher'] = vouchers
        return render(request, 'voucher.html', response)
    else:
        return HttpResponseRedirect('/')

def tambah_voucher(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path to bike_sharing;")
    if (request.method == "POST"):
        namaVoucher = request.POST['nama_voucher']
        kategoriVoucher = request.POST['kategori']
        nilaiPoin = request.POST['nilai_poin']
        deskripsiVoucher = request.POST['deskripsi']
        jumlahVoucher= request.POST['jumlah']

        if (int(jumlahVoucher) > 1):
            cursor.execute("SELECT id_voucher FROM voucher ORDER BY cast (id_voucher AS INT) DESC;")
            hasil = cursor.fetchone()
            id_voucher = int(hasil[0]) + 1
            for i in range(int(jumlahVoucher)):
                cursor.execute("INSERT INTO voucher(id_voucher,nama,kategori,nilai_poin,deskripsi) VALUES(" + "'" + str(id_voucher) + "','" + namaVoucher + "','" + kategoriVoucher + "'," + nilaiPoin + ",'" + deskripsiVoucher + "'" + ");")
                id_voucher += 1
        else:
            cursor.execute("SELECT id_voucher FROM voucher ORDER BY cast (id_voucher AS INT) DESC;")
            hasil = cursor.fetchone()
            id_voucher = int(hasil[0]) + 1
            cursor.execute("INSERT INTO voucher(id_voucher,nama,kategori,nilai_poin,deskripsi) VALUES("+"'"+str(id_voucher)+"','"+namaVoucher+"','"+kategoriVoucher+"',"+nilaiPoin+",'"+deskripsiVoucher+"'"+");")

        messages.error(request, "Voucher telah ditambahkan!")
        return HttpResponseRedirect('/voucher/daftar-voucher')
    else:
        messages.error(request, "Voucher gagal ditambahkan.")
        return HttpResponseRedirect('/voucher/daftar-voucher')

def daftar_voucher(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path to bike_sharing;")
    response = {}
    cursor.execute("SELECT * FROM voucher")
    vouchers = cursor.fetchall()
    response['daftar_voucher'] = vouchers
    print(response)
    return render(request, 'daftar_voucher.html', response)

@csrf_exempt
def update_voucher(request):
    if (request.method == "POST"):
        id_voucher = request.POST['id']
        namaVoucher = request.POST['nama_voucher']
        kategoriVoucher = request.POST['kategori']
        nilaiPoin = request.POST['nilai_poin']
        deskripsiVoucher = request.POST['deskripsi']
        cursor = connection.cursor()
        cursor.execute("SET search_path to bike_sharing;")
        cursor.execute("update voucher set nama='" + namaVoucher + "', kategori='" +
            kategoriVoucher + "', nilai_poin='" + nilaiPoin + "', deskripsi='" + deskripsiVoucher + "' where id_voucher = '" + id_voucher + "';")
        messages.error(request, "Voucher berhasil diperbarui!")
        return HttpResponseRedirect('/voucher/update-voucher')

    else:
        messages.error(request, "Voucher gagal diperbarui.")
        return HttpResponseRedirect('/voucher/daftar-voucher')

@csrf_exempt
def delete_voucher(request):
    if (request.method == "POST"):
        id_voucher = request.POST['id']
        cursor = connection.cursor()
        cursor.execute("SET search_path to bike_sharing;")
        cursor.execute("DELETE FROM voucher WHERE id_voucher='"+id_voucher+"';")
        messages.error(request, "Voucher berhasil dihapuskan!")
        return HttpResponseRedirect('/voucher/daftar-voucher')

    else:
        messages.error(request, "Voucher gagal dihapuskan.")
        return HttpResponseRedirect('/voucher/daftar-voucher')

@csrf_exempt
def detail_update_voucher(request):
    if (request.method == "POST"):
        response = {}
        voucher = request.POST['id']
        cursor = connection.cursor()
        cursor.execute("SET search_path to bike_sharing;")
        cursor.execute("SELECT * FROM voucher WHERE id_voucher='" + voucher + "';")
        id_voucher = cursor.fetchone()
        cursor.execute("SELECT kategori FROM voucher")
        kategori = cursor.fetchall()
        response['kategori'] = kategori
        response['voucher'] = id_voucher
        messages.error(request, "success")
        return JsonResponse(response)
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/voucher/daftar-voucher')

