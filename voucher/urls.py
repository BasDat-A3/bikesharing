from django.urls import path
from voucher import views

urlpatterns = [
	path('', views.voucher, name='voucher'),
	path('/tambah-voucher', views.tambah_voucher, name='tambah_voucher'),
	path('/daftar-voucher', views.daftar_voucher, name='daftar_voucher'),
	path('/update-voucher', views.update_voucher, name='update_voucher'),
	path('/delete-voucher', views.delete_voucher, name='delete_voucher'),
	path('/detail-update-voucher', views.detail_update_voucher, name='detail_update_voucher'),
]
