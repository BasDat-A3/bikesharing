## BikeSharingA3 - TK Basis Data 2019 Kelas A Kelompok 3
## Nama Anggota Kelompok
- Muhammad Khatami (1706044055)
- Putri Kinanti Ramadhani B. (1706022810)
- Ringgi Cahyo Dwiputra (1706025005)
- Salsabila Maurizka (1706043986)

## Link Herokuapp
Link: https://basdat-a3-bikesharing.herokuapp.com/

## Pembagian Tugas
- Muhammad Khatami (Fitur 1-4)
- Putri Kinanti Ramadhani B. (Fitur 5-8)
- Ringgi Cahyo Dwiputra (Fitur 13-16)
- Salsabila Maurizka (Fitur 9-12)