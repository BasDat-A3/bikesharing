function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("td")[n];
      y = rows[i + 1].getElementsByTagName("td")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function deleteList(x){
    $(".cd-popup").addClass("is-visible")
    $(".cd-popup").attr('id',x)
}

function updateVoucher(){
    // length = $(".stasiun").attr('id')
    id_voucher = $(".modal-body").attr('id')
    nama = $('#namaVoucher').val()
    if(nama==''){
        nama = $('#namaVoucher').attr('placeholder')
    }
    kategori = $('#kategoriVoucher').val()
    if(kategori==''){
        kategori=$('#kategoriVoucher').attr('placeholder')
    }
    poin = $('#poinVoucher').val()
    if(poin==''){
        poin = $('#poinVoucher').attr('placeholder')
    }
    deskripsi = $('#deskripsiVoucher').val()
    if(deskripsi==''){
        deskripsi = $('#deskripsiVoucher').attr('placeholder')
    }
    jumlah = $('#jumlahVoucher').val()
    if(jumlah==''){
        jumlah = $('#jumlahVoucher').attr('placeholder')
    }
    alert(jumlah)

    all_data={
        'id_voucher':id_voucher,
        'nama':nama,
        'kategori':kategori,
        'poin':poin,
        'deskripsi':deskripsi,
        'jumlah':jumlah,
    }
    console.log(all_data)

    $.ajax(
        {
          url: "/voucher/update-voucher/",
            datatype: 'json',
            data: all_data,
          method:'POST',
          success: function(result){
            location.reload()
            } ,
            error:function(a){
                console.log(a)
                location.reload();
            }
        }
    );
}

function cancelFunction(){
    $('.cd-popup').removeClass('is-visible')
}

function yesFunction(){
    id = $(".cd-popup").attr('id')
    console.log("hello")
    $.ajax(
        {
          url: "/voucher/delete-voucher/",
            datatype: 'json',
            data: {'id':id},
          method:'POST',
          success: function(result){
            location.reload()
            }
        }
        );
}

function updateModal(x){
    $(".modal-body").attr('id',x)
    $(".modal-body").attr('id',x)
    $(".spinner-border").addClass("true")
    $(".spinner-border").removeClass("false")
    $("#load").addClass("false")
    $("#load").removeClass("true")
    $.ajax(
        {
          url: "/voucher/detail-update-voucher/",
            datatype: 'json',
            data: {'id':x},
          method:'POST',
          success: function(result){
            $(".spinner-border").addClass("false")
            $(".spinner-border").removeClass("true")
            $("#load").addClass("true")
            $("#load").removeClass("false")
            all_kategori=''
            for(i=0; i<result.kategori.length;i++){
                all_kategori+='<option>'+result.kategori[i]+'</option>'
            }
            kategori=''
            for(j=0;j<result.kategori.length;j++){
                kategori+=`<label for="formGroupExampleInput2">Kategori</label>
                <select name="kategori" id="kategoriVoucher`+j+`" class="form-control"placeholder="Kategori"required>
                    <option selected>`+result.kategori[j]+`</option>`+
                    all_kategori+`
                </select>`
            }
            $('#namaVoucher').attr('placeholder',result.voucher[1])
            $('#jumlahVoucher').attr('placeholder',result.voucher[6])
            $('#poinVoucher').attr('placeholder',result.voucher[3])
            $('#deskripsiVoucher').attr('placeholder',result.voucher[4])
            $('#kategoriVoucher').html(kategori)
            $('.voucher').attr('id',result.kategori.length)
            }
        });
}

$(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
