from django.urls import path
from transaksi import views

urlpatterns = [
	path('riwayat/', views.riwayat, name = 'riwayat'),
	path('topup/', views.topup, name = 'topup'),	
	path('afterTopup/', views.afterTopup, name = 'afterTopup'),	
]
