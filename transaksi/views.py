from django.shortcuts import render
from django.db import connection
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
import datetime

# Create your views here.
def riwayat(request):
    ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("select t.date_time,t.jenis,t.nominal from anggota as a, transaksi as t where a.ktp='"+ktp+"' and t.no_kartu_anggota = a.no_kartu;")
    allTransaksi = cursor.fetchall()

    laporanDict = {'all_posts': allTransaksi}
    print(allTransaksi)
    return render(request, 'riwayat.html', laporanDict)

def topup(request):
    return render(request, 'topup.html')    

def afterTopup(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing, public')
    if (request.method == "POST"):
        inputSaldo = str(request.POST.get('topup'))
        ktp = request.session['ktp']
        cursor.execute("select no_kartu from anggota where ktp = '" + ktp + "'")
        no_kartu = cursor.fetchall()
        for row in no_kartu:
            no_kartu = row[0]

        now = datetime.datetime.now()
        now = str(now)
        print("==============")
        print(now)

        cursor.execute("select saldo from anggota where ktp = '" + ktp + "'")
        saldoAwal = cursor.fetchall()
        print(saldoAwal)
        for row in saldoAwal:
            saldoAwal = row[0]
        saldoAkhir = int(inputSaldo) + int(saldoAwal)
        saldoAkhir = str(saldoAkhir)
        cursor.execute("update anggota set saldo = '" + saldoAkhir + "' where ktp = '" + ktp + "'")
        cursor.execute("INSERT INTO TRANSAKSI (no_kartu_anggota, date_time, jenis, nominal) VALUES (" + no_kartu + ",'"+ now +"','topup'," + inputSaldo + ");")
        messages.success(request, "topup berhasil")
        return HttpResponseRedirect('/transaksi/riwayat/')
    else :
        messages.error(request, "topup gagal")
        return HttpResponseRedirect('/transaksi/topup/')
